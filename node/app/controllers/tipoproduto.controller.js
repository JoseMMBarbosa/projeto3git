const Tipoproduto = require('../models/tipoproduto.js');

const mongoose = require('mongoose');

const SubCat = require('../models/subcategoria.js');

const Qualificacao = require('../models/qualificacao.js');

// Create and Save a new Note
exports.create = (req, res) => {
    // Validate request
    if(!req.body.nome) {
        return res.status(400).send({
            nome: "Note content can not be lol"
        });
    }

    SubCat.findOne({_id: req.body.idSubcategoria}, function(err, result) {
        if (err) {
            throw err};
        console.log(result._id);

        Qualificacao.findOne({_id: req.body.idQualificacao}, function(err, result1) {
            if (err) {
                throw err};
            console.log(result1._id);
    // Create a Note
    const tipoproduto = new Tipoproduto({
        _id: mongoose.Types.ObjectId(),
        idSubcategoria: result._id,
        idQualificacao: result1._id,
        nome: req.body.nome, 
        designacao: req.body.designacao

    });

    // Save Note in the database
    tipoproduto.save()
    .then(data => {
        res.send(data);
    }).catch(err => {
        res.status(500).send({
            message: err.message || "Some error occurred while creating the Note."
        });
    });
});
});
};

// Retrieve and return all notes from the database.
exports.findAll = (req, res) => {
    Tipoproduto.find().populate('idQualificacao').populate('idSubcategoria')
    .then(tipoprodutos => {
        res.send(tipoprodutos);
    }).catch(err => {
        res.status(500).send({
            message: err.message || "Some error occurred while retrieving notes."
        });
    });
};

exports.findOne = (req, res) => {
    Tipoproduto.findById(req.params.tipoprodutoId)
    .then(tipoproduto => {
        if(!tipoproduto) {
            return res.status(404).send({
                message: "cliente not found with id " + req.params.tipoprodutoId
            });            
        }
        res.send(tipoproduto);
    }).catch(err => {
        if(err.kind === 'ObjectId') {
            return res.status(404).send({
                message: "cliente not found with id " + req.params.tipoprodutoId
            });                
        }
        return res.status(500).send({
            message: "Error retrieving cliente with id " + req.params.tipoprodutoId
        });
    });
};

// Update a note identified by the noteId in the request
exports.update = (req, res) => {
    // Validate Request
    if(!req.body.content) {
        return res.status(400).send({
            message: "cliente content can not be empty"
        });
    }

    // Find note and update it with the request body
    Tipoproduto.findByIdAndUpdate(req.params.tipoprodutoId, {
        title: req.body.title || "Untitled Note",
        content: req.body.content
    }, {new: true})
    .then(tipoproduto => {
        if(!tipoproduto) {
            return res.status(404).send({
                message: "Note not found with id " + req.params.tipoprodutoId
            });
        }
        res.send(tipoproduto);
    }).catch(err => {
        if(err.kind === 'ObjectId') {
            return res.status(404).send({
                message: "Note not found with id " + req.params.tipoprodutoId
            });                
        }
        return res.status(500).send({
            message: "Error updating note with id " + req.params.tipoprodutoId
        });
    });
}

exports.deleteOne = (req, res) => {
    Tipoproduto.deleteOne({_id: req.body.tipoProdutoId})
    .then(tipoproduto => {
        if(!tipoproduto) {
            return res.status(404).send({
                message: "Note not found with id " + req.body.tipoProdutoId
            });
        }
        res.send({message: "Note deleted successfully!"});
    }).catch(err => {
        if(err.kind === 'ObjectId' || err.name === 'NotFound') {
            return res.status(404).send({
                message: "Note not found with id " + req.body.tipoProdutoId
            });                
        }
        return res.status(500).send({
            message: "Could not delete note with id " + req.body.tipoProdutoId
        });
    });
};

// Delete a note with the specified noteId in the request
exports.delete = (req, res) => {
    Tipoproduto.findByIdAndRemove(req.params.tipoProdutoId)
    .then(tipoproduto => {
        if(!tipoproduto) {
            return res.status(404).send({
                message: "Note not found with id " + req.params.tipoProdutoId
            });
        }
        res.send({message: "Note deleted successfully!"});
    }).catch(err => {
        if(err.kind === 'ObjectId' || err.name === 'NotFound') {
            return res.status(404).send({
                message: "Note not found with id " + req.params.tipoProdutoId
            });                
        }
        return res.status(500).send({
            message: "Could not delete note with id " + req.params.tipoProdutoId
        });
    });
};