const Entidadecertificacao = require('../models/entidadecertificacao.js');

const mongoose = require('mongoose');


// Create and Save a new Note
exports.create = (req, res) => {
    // Validate request
    if(!req.body.nome) {
        return res.status(400).send({
            nome: "Note content can not be lol"
        });
    }

    // Create a Note
    const entidadecertificacao = new Entidadecertificacao({
        _id: mongoose.Types.ObjectId(),
        nome: req.body.nome, 
        descricao: req.body.descricao
    });

    // Save Note in the database
    entidadecertificacao.save()
    .then(data => {
        res.send(data);
    }).catch(err => {
        res.status(500).send({
            message: err.message || "Some error occurred while creating the Note."
        });
    });
};

// Retrieve and return all notes from the database.
exports.findAll = (req, res) => {
    Entidadecertificacao.find()
    .then(entidadecertificacoes => {
        res.send(entidadecertificacoes);
    }).catch(err => {
        res.status(500).send({
            message: err.message || "Some error occurred while retrieving notes."
        });
    });
};

exports.deleteOne = (req, res) => {
    Entidadecertificacao.remove({_id: req.body.entitadeId})
    .then(entidadecertificacoes => {
        if(!entidadecertificacoes) {
            return res.status(404).send({
                message: "Note not found with id " + req.body.entitadeId
            });
        }
        res.send({message: "Note deleted successfully!"});
    }).catch(err => {
        if(err.kind === 'ObjectId' || err.name === 'NotFound') {
            return res.status(404).send({
                message: "Note not found with id " + req.body.entitadeId
            });                
        }
        return res.status(500).send({
            message: "Could not delete note with id " + req.body.entitadeId
        });
    });
};



exports.findOne = (req, res) => {
    Entidadecertificacao.findById(req.params.EntidadecertificacaoId)
    .then(entidadecertificacao => {
        if(!entidadecertificacao) {
            return res.status(404).send({
                message: "cliente not found with id " + req.params.EntidadecertificacaoId
            });            
        }
        res.send(entidadecertificacao);
    }).catch(err => {
        if(err.kind === 'ObjectId') {
            return res.status(404).send({
                message: "cliente not found with id " + req.params.EntidadecertificacaoId
            });                
        }
        return res.status(500).send({
            message: "Error retrieving cliente with id " + req.params.EntidadecertificacaoId
        });
    });
};

// Update a note identified by the noteId in the request
exports.update = (req, res) => {
    // Validate Request
    if(!req.body.content) {
        return res.status(400).send({
            message: "cliente content can not be empty"
        });
    }

    // Find note and update it with the request body
    Entidadecertificacao.findByIdAndUpdate(req.params.EntidadecertificacaoId, {
        title: req.body.title || "Untitled Note",
        content: req.body.content
    }, {new: true})
    .then(entidadecertificacao => {
        if(!entidadecertificacao) {
            return res.status(404).send({
                message: "Note not found with id " + req.params.EntidadecertificacaoId
            });
        }
        res.send(entidadecertificacao);
    }).catch(err => {
        if(err.kind === 'ObjectId') {
            return res.status(404).send({
                message: "Note not found with id " + req.params.EntidadecertificacaoId
            });                
        }
        return res.status(500).send({
            message: "Error updating note with id " + req.params.EntidadecertificacaoId
        });
    });
}

// Delete a note with the specified noteId in the request
exports.delete = (req, res) => {
    Entidadecertificacao.findByIdAndRemove(req.params.EntidadecertificacaoId)
    .then(entidadecertificacao => {
        if(!entidadecertificacao) {
            return res.status(404).send({
                message: "Note not found with id " + req.params.EntidadecertificacaoId
            });
        }
        res.send({message: "Note deleted successfully!"});
    }).catch(err => {
        if(err.kind === 'ObjectId' || err.name === 'NotFound') {
            return res.status(404).send({
                message: "Note not found with id " + req.params.EntidadecertificacaoId
            });                
        }
        return res.status(500).send({
            message: "Could not delete note with id " + req.params.EntidadecertificacaoId
        });
    });
};