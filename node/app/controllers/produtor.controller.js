const Produtor = require('../models/produtor.js');

const mongoose = require('mongoose');

const local = require('../models/local.js');
// Create and Save a new Note
exports.create = (req, res) => {
    // Validate request
    if(!req.body.nome) {
        return res.status(400).send({
            nome: "Note content can not be lol"
        });
    }

    local.findOne({_id: req.body.idLocal}, function(err, result) {
        if (err) {
            throw err};
        console.log(result._id);

        // Create a Note
        const produtor = new Produtor({
        _id: mongoose.Types.ObjectId(),
        idLocal: result._id,
        nome: req.body.nome, 
        contacto: req.body.contacto,
        descricao: req.body.descricao, 
        email: req.body.email,
        nif: req.body.nif

        });

        // Save Note in the database
        produtor.save()
        .then(data => {
            res.send(data);
        }).catch(err => {
            res.status(500).send({
                message: err.message || "Some error occurred while creating the Note."
            });
        });
    });
};

exports.deleteOne = (req, res) => {
    Produtor.remove({_id: req.body.produtorId})
    .then(produtor => {
        if(!produtor) {
            return res.status(404).send({
                message: "Note not found with id " + req.params.produtorId
            });
        }
        res.send({message: "Note deleted successfully!"});
    }).catch(err => {
        if(err.kind === 'ObjectId' || err.name === 'NotFound') {
            return res.status(404).send({
                message: "Note not found with id " + req.params.produtorId
            });                
        }
        return res.status(500).send({
            message: "Could not delete note with id " + req.params.produtorId
        });
    });
};


exports.updateProdutor = (req, res) => {
    // Validate Request
    if(!req.body._id) {
        return res.status(400).send({
            message: "cliente content can not be empty"
        });
    }

    // Find note and update it with the request body
    Produtor.findByIdAndUpdate(req.body._id, {
        nome: req.body.nome,
        //idLocal: req.body.idLocal,
        //contacto: req.body.contacto,
        //nif: req.body.nif
    }, {new: true})
    .then(produtores => {
        if(!produtores) {
            return res.status(404).send({
                message: "Note not found with id " + req.body._id
            });
        }
        res.send(produtores);
    }).catch(err => {
        if(err.kind === 'ObjectId') {
            return res.status(404).send({
                message: "Note not found with id " + req.body._id
            });                
        }
        return res.status(500).send({
            message: "Error updating note with id " + req.body._id
        });
    });
}

exports.updateFinal = (req, res) => {
    // Validate Request
    // Find note and update it with the request body
    Produtor.findByIdAndUpdate(req.body._id, {
        nome: req.body.nome,
        idLocal: req.body.idLocal,
        contacto: req.body.contacto,
        nif: req.body.nif,
    }, {new: true})
    .then(produtores => {
        if(!produtores) {
            return res.status(404).send({
                message: "Note not found with id " + req.body._id
            });
        }
        console.log(ola);
        res.send(produtores);
    }).catch(err => {
        if(err.kind === 'ObjectId') {
            return res.status(404).send({
                message: "Note not found with id " + req.body._id
            });                
        }
    });
}


// Retrieve and return all notes from the database.
exports.findAll = (req, res) => {
    Produtor.find().populate('idLocal')
    .then(produtores => {
        res.send(produtores);
    }).catch(err => {
        res.status(500).send({
            message: err.message || "Some error occurred while retrieving notes."
        });
    });
};


exports.findOne = (req, res) => {
    Produtor.findById(req.params.produtorId)
    .then(produtor => {
        if(!produtor) {
            return res.status(404).send({
                message: "cliente not found with id " + req.params.produtorId
            });            
        }
        res.send(produtor);
    }).catch(err => {
        if(err.kind === 'ObjectId') {
            return res.status(404).send({
                message: "cliente not found with id " + req.params.produtorId
            });                
        }
        return res.status(500).send({
            message: "Error retrieving cliente with id " + req.params.produtorId
        });
    });
};

// Update a note identified by the noteId in the request
exports.update = (req, res) => {
    // Validate Request
    if(!req.body.content) {
        return res.status(400).send({
            message: "cliente content can not be empty"
        });
    }

    // Find note and update it with the request body
    Produtor.findByIdAndUpdate(req.params.produtorId, {
        title: req.body.title,
        idLocal: req.body.content,
        contacto: req.body.contacto,
        nif: req.body.nif
    }, {new: true})
    .then(produtor => {
        if(!produtor) {
            return res.status(404).send({
                message: "Note not found with id " + req.params.produtorId
            });
        }
        res.send(produtor);
    }).catch(err => {
        if(err.kind === 'ObjectId') {
            return res.status(404).send({
                message: "Note not found with id " + req.params.produtorId
            });                
        }
        return res.status(500).send({
            message: "Error updating note with id " + req.params.produtorId
        });
    });
}

// Delete a note with the specified noteId in the request
exports.delete = (req, res) => {
    Produtor.findByIdAndRemove(req.params.produtorId)
    .then(produtor => {
        if(!produtor) {
            return res.status(404).send({
                message: "Note not found with id " + req.params.produtorId
            });
        }
        res.send({message: "Note deleted successfully!"});
    }).catch(err => {
        if(err.kind === 'ObjectId' || err.name === 'NotFound') {
            return res.status(404).send({
                message: "Note not found with id " + req.params.produtorId
            });                
        }
        return res.status(500).send({
            message: "Could not delete note with id " + req.params.produtorId
        });
    });
};