const Subcategoria = require('../models/subcategoria.js');

const mongoose = require('mongoose');

const categoria = require('../models/categoria.js')

// Create and Save a new Note
exports.create = (req, res) => {
    // Validate request
    if(!req.body.idCategoria) {
        return res.status(400).send({
            nome: "Note content can not be lol"
        });
    }
    
    categoria.findOne({_id: req.body.idCategoria}, function(err, result) {
        if (err) {
            throw err};
        console.log(result._id);

    // Create a Note
    const subcategoria = new Subcategoria({
        _id: mongoose.Types.ObjectId(),
        idCategoria: result._id,
        nome: req.body.nome,
        designacao: req.body.designacao

    });

    // Save Note in the database
    subcategoria.save()
    .then(data => {
        res.send(data);
    }).catch(err => {
        res.status(500).send({
            message: err.message || "Some error occurred while creating the Note."
        });
    });
});
};

// Retrieve and return all notes from the database.
exports.findAll = (req, res) => {
    Subcategoria.find().populate('idCategoria')
    .then(subcategorias => {
        res.send(subcategorias);
    }).catch(err => {
        res.status(500).send({
            message: err.message || "Some error occurred while retrieving notes."
        });
    });
};

exports.deleteOne = (req, res) => {
    Subcategoria.remove({_id: req.body.subCategoriaId})
    .then(subcategorias => {
        if(!subcategorias) {
            return res.status(404).send({
                message: "Note not found with id " + req.body.subCategoriaId
            });
        }
        res.send({message: "Note deleted successfully!"});
    }).catch(err => {
        if(err.kind === 'ObjectId' || err.name === 'NotFound') {
            return res.status(404).send({
                message: "Note not found with id " + req.body.subCategoriaId
            });                
        }
        return res.status(500).send({
            message: "Could not delete note with id " + req.body.subCategoriaId
        });
    });
};

exports.findOne = (req, res) => {
    Subcategoria.findById(req.params.subcategoriaId)
    .then(subcategoria => {
        if(!subcategoria) {
            return res.status(404).send({
                message: "cliente not found with id " + req.params.subcategoriaId
            });            
        }
        res.send(subcategoria);
    }).catch(err => {
        if(err.kind === 'ObjectId') {
            return res.status(404).send({
                message: "cliente not found with id " + req.params.subcategoriaId
            });                
        }
        return res.status(500).send({
            message: "Error retrieving cliente with id " + req.params.subcategoriaId
        });
    });
};

// Update a note identified by the noteId in the request
exports.update = (req, res) => {
    // Validate Request
    if(!req.body.content) {
        return res.status(400).send({
            message: "cliente content can not be empty"
        });
    }

    // Find note and update it with the request body
    Subcategoria.findByIdAndUpdate(req.params.subcategoriaId, {
        title: req.body.title || "Untitled Note",
        content: req.body.content
    }, {new: true})
    .then(subcategoria => {
        if(!subcategoria) {
            return res.status(404).send({
                message: "Note not found with id " + req.params.subcategoriaId
            });
        }
        res.send(subcategoria);
    }).catch(err => {
        if(err.kind === 'ObjectId') {
            return res.status(404).send({
                message: "Note not found with id " + req.params.subcategoriaId
            });                
        }
        return res.status(500).send({
            message: "Error updating note with id " + req.params.subcategoriaId
        });
    });
}

// Delete a note with the specified noteId in the request
exports.delete = (req, res) => {
    Subcategoria.findByIdAndRemove(req.params.subcategoriaId)
    .then(subcategoria => {
        if(!subcategoria) {
            return res.status(404).send({
                message: "Note not found with id " + req.params.subcategoriaId
            });
        }
        res.send({message: "Note deleted successfully!"});
    }).catch(err => {
        if(err.kind === 'ObjectId' || err.name === 'NotFound') {
            return res.status(404).send({
                message: "Note not found with id " + req.params.subcategoriaId
            });                
        }
        return res.status(500).send({
            message: "Could not delete note with id " + req.params.subcategoriaId
        });
    });
};