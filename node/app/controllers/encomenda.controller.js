const Encomenda = require('../models/encomenda.js');

const mongoose = require('mongoose');

const LinhaEnc = require('../models/linhaenc.js')

const User = require('../models/users.js')

// Create and Save a new Note
exports.create = (req, res) => {
    // Validate request
    if(!req.body.email) {
        return res.status(400).send({
            nome: "Note content can not be lol"
        });
    }
    // Create a Note
    const encomenda = new Encomenda({
        _id: mongoose.Types.ObjectId(),
        email: req.body.email,
        precociva: req.body.precociva,
        total: req.body.total,
        data: Date.now(),
        pago: "true",
    });

    // Save Note in the database
    encomenda.save()
    .then(data => {
        res.send(data);
    }).catch(err => {
        res.status(500).send({
            message: err.message || "Some error occurred while creating the Note."
        });
    });
};

// Retrieve and return all notes from the database.
exports.findAll = (req, res) => {
    Encomenda.find()
    .then(encomendas => {
        res.send(encomendas);
    }).catch(err => {
        res.status(500).send({
            message: err.message || "Some error occurred while retrieving notes."
        });
    });
};

exports.encomendaCliente = (req, res) => {
    User.findOne({email: req.body.email}, function(err, result) {
        if (err) {
            return res.status(500).send({
                message: err.message || "Some error occurred while retrieving notes."
            });
        } else if (result) {        
            console.log(result.email)
            Encomenda.find({email: result.email})
            .then(encomendas => {
                res.send(encomendas);
            }).catch(err => {
                res.status(500).send({
                    message: err.message || "Some error occurred while retrieving notes."
                });
            });
        }
    });
};
/*
exports.findOne = (req, res) => {
    Encomenda.findById(req.params.encomendaId)
    .then(encomenda => {
        if(!encomenda) {
            return res.status(404).send({
                message: "cliente not found with id " + req.params.encomendaId
            });            
        }
        res.send(encomenda);
    }).catch(err => {
        if(err.kind === 'ObjectId') {
            return res.status(404).send({
                message: "cliente not found with id " + req.params.encomendaId
            });                
        }
        return res.status(500).send({
            message: "Error retrieving cliente with id " + req.params.encomendaId
        });
    });
};
*/

// Update a note identified by the noteId in the request
exports.update = (req, res) => {
    // Validate Request
    if(!req.body.content) {
        return res.status(400).send({
            message: "cliente content can not be empty"
        });
    }

    // Find note and update it with the request body
    Encomenda.findByIdAndUpdate(req.params.encomendaId, {
        title: req.body.title || "Untitled Note",
        content: req.body.content
    }, {new: true})
    .then(encomenda => {
        if(!encomenda) {
            return res.status(404).send({
                message: "Note not found with id " + req.params.encomendaId
            });
        }
        res.send(encomenda);
    }).catch(err => {
        if(err.kind === 'ObjectId') {
            return res.status(404).send({
                message: "Note not found with id " + req.params.encomendaId
            });                
        }
        return res.status(500).send({
            message: "Error updating note with id " + req.params.encomendaId
        });
    });
}

// Delete a note with the specified noteId in the request
exports.delete = (req, res) => {
    Encomenda.findByIdAndRemove(req.params.encomendaId)
    .then(encomenda => {
        if(!encomenda) {
            return res.status(404).send({
                message: "Note not found with id " + req.params.encomendaId
            });
        }
        res.send({message: "Note deleted successfully!"});
    }).catch(err => {
        if(err.kind === 'ObjectId' || err.name === 'NotFound') {
            return res.status(404).send({
                message: "Note not found with id " + req.params.encomendaId
            });                
        }
        return res.status(500).send({
            message: "Could not delete note with id " + req.params.encomendaId
        });
    });
};