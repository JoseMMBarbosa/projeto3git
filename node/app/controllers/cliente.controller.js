
const Cliente = require('../models/cliente.js');

const mongoose = require('mongoose');

// Create and Save a new Note
exports.create = (req, res) => {
    // Validate request
    if(!req.body.email) {
        return res.status(400).send({
            nome: "Note content can not be lol"
        });
    }

    // Create a Note
    const cliente = new Cliente({
        _id: mongoose.Types.ObjectId(),
        nome: req.body.nome, 
        morada: req.body.morada, 
        localidade: req.body.localidade,
        codigopostal: req.body.codigopostal,
        contacto: req.body.contacto,
        email: req.body.email,
        nif: req.body.nif

    });

    // Save Note in the database
    cliente.save()
    .then(data => {
        res.send(data);
    }).catch(err => {
        res.status(500).send({
            message: err.message || "Some error occurred while creating the Note."
        });
    });
};

// Retrieve and return all notes from the database.
exports.findAll = (req, res) => {
    Cliente.find()
    .then(clientes => {
        res.send(clientes);
    }).catch(err => {
        res.status(500).send({
            message: err.message || "Some error occurred while retrieving notes."
        });
    });
};

exports.findOne = (req, res) => {
    Cliente.findById(req.params.clienteId)
    .then(cliente => {
        if(!cliente) {
            return res.status(404).send({
                message: "cliente not found with id " + req.params.clienteId
            });            
        }
        res.send(cliente);
    }).catch(err => {
        if(err.kind === 'ObjectId') {
            return res.status(404).send({
                message: "cliente not found with id " + req.params.clienteId
            });                
        }
        return res.status(500).send({
            message: "Error retrieving cliente with id " + req.params.clienteId
        });
    });
};

// Update a note identified by the noteId in the request
exports.update = (req, res) => {
    // Validate Request
    if(!req.body.content) {
        return res.status(400).send({
            message: "cliente content can not be empty"
        });
    }

    // Find note and update it with the request body
    Cliente.findByIdAndUpdate(req.params.clienteId, {
        title: req.body.title || "Untitled Note",
        content: req.body.content
    }, {new: true})
    .then(cliente => {
        if(!noclientete) {
            return res.status(404).send({
                message: "Note not found with id " + req.params.clienteId
            });
        }
        res.send(cliente);
    }).catch(err => {
        if(err.kind === 'ObjectId') {
            return res.status(404).send({
                message: "Note not found with id " + req.params.clienteId
            });                
        }
        return res.status(500).send({
            message: "Error updating note with id " + req.params.clienteId
        });
    });
}

// Delete a note with the specified noteId in the request
exports.delete = (req, res) => {
    Cliente.findByIdAndRemove(req.params.clienteId)
    .then(cliente => {
        if(!cliente) {
            return res.status(404).send({
                message: "Note not found with id " + req.params.clienteId
            });
        }
        res.send({message: "Note deleted successfully!"});
    }).catch(err => {
        if(err.kind === 'ObjectId' || err.name === 'NotFound') {
            return res.status(404).send({
                message: "Note not found with id " + req.params.clienteId
            });                
        }
        return res.status(500).send({
            message: "Could not delete note with id " + req.params.clienteId
        });
    });
};