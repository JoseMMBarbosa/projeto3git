const Local = require('../models/local.js');

const mongoose = require('mongoose');

// Create and Save a new Note
exports.create = (req, res) => {
    // Validate request
    if(!req.body.nome) {
        return res.status(400).send({
            nome: "Note content can not be lol"
        });
    }

    // Create a Note
    const local = new Local({
        _id: mongoose.Types.ObjectId(),
        nome: req.body.nome, 
        distrito: req.body.distrito,
        codigopostal: req.body.codigopostal,
        //latitude: req.body.latitude,
        //longitude: req.body.longitude
    });

    // Save Note in the database
    local.save()
    .then(data => {
        res.send(data);
    }).catch(err => {
        res.status(500).send({
            message: err.message || "Some error occurred while creating the Note."
        });
    });
};

// Retrieve and return all notes from the database.
exports.findAll = (req, res) => {
    Local.find()
    .then(locals => {
        res.send(locals);
    }).catch(err => {
        res.status(500).send({
            message: err.message || "Some error occurred while retrieving notes."
        });
    });
};

exports.deleteOne = (req, res) => {
    Local.remove({_id: req.body.locaId})
    .then(local => {
        if(!local) {
            return res.status(404).send({
                message: "Note not found with id " + req.body.locaId
            });
        }
        res.send({message: "Note deleted successfully!"});
    }).catch(err => {
        if(err.kind === 'ObjectId' || err.name === 'NotFound') {
            return res.status(404).send({
                message: "Note not found with id " + req.body.locaId
            });                
        }
        return res.status(500).send({
            message: "Could not delete note with id " + req.body.locaId
        });
    });
};

exports.findOne = (req, res) => {
    Local.findById(req.params.localId)
    .then(local => {
        if(!local) {
            return res.status(404).send({
                message: "cliente not found with id " + req.params.localId
            });            
        }
        res.send(local);
    }).catch(err => {
        if(err.kind === 'ObjectId') {
            return res.status(404).send({
                message: "cliente not found with id " + req.params.localId
            });                
        }
        return res.status(500).send({
            message: "Error retrieving cliente with id " + req.params.localId
        });
    });
};

// Update a note identified by the noteId in the request
exports.update = (req, res) => {
    // Validate Request
    if(!req.body.content) {
        return res.status(400).send({
            message: "cliente content can not be empty"
        });
    }

    // Find note and update it with the request body
    Local.findByIdAndUpdate(req.params.localId, {
        title: req.body.title || "Untitled Note",
        content: req.body.content
    }, {new: true})
    .then(local => {
        if(!local) {
            return res.status(404).send({
                message: "Note not found with id " + req.params.localId
            });
        }
        res.send(local);
    }).catch(err => {
        if(err.kind === 'ObjectId') {
            return res.status(404).send({
                message: "Note not found with id " + req.params.localId
            });                
        }
        return res.status(500).send({
            message: "Error updating note with id " + req.params.localId
        });
    });
}

// Delete a note with the specified noteId in the request
exports.delete = (req, res) => {
    Local.findByIdAndRemove(req.params.localId)
    .then(local => {
        if(!local) {
            return res.status(404).send({
                message: "Note not found with id " + req.params.localId
            });
        }
        res.send({message: "Note deleted successfully!"});
    }).catch(err => {
        if(err.kind === 'ObjectId' || err.name === 'NotFound') {
            return res.status(404).send({
                message: "Note not found with id " + req.params.localId
            });                
        }
        return res.status(500).send({
            message: "Could not delete note with id " + req.params.localId
        });
    });
};