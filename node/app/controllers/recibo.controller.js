const Recibo = require('../models/recibo.js');

const mongoose = require('mongoose');

const Cliente = require('../models/cliente.js');

const Encomenda = require('../models/recibo.js');

// Create and Save a new Note
exports.create = (req, res) => {
    // Validate request
    if(!req.body.email) {
        return res.status(400).send({
            message: "Note content can not be empty"
        });
    }
    

    // create
    const recibo = new Recibo({
        _id: mongoose.Types.ObjectId(),
        //id Encomenda
        //idCliente
        datapagamento: req.body.datapagamento,
        metodopagamento: req.body.metodopagamento
    });

    // Save Note in the database
    recibo.save()
    .then(data => {
        res.send(data);
    }).catch(err => {
        res.status(500).send({
            message: err.message || "Some error occurred while creating the Note."
        });
    });
};

// Retrieve and return all notes from the database.
exports.findAll = (req, res) => {
    Recibo.find()
    .then(recibos => {
        res.send(recibos);
    }).catch(err => {
        res.status(500).send({
            message: err.message || "Some error occurred while retrieving notes."
        });
    });
};

exports.findOne = (req, res) => {
    User.findById(req.params.reciboId)
    .then(recibo => {
        if(!recibo) {
            return res.status(404).send({
                message: "Note not found with id " + req.params.reciboId
            });            
        }
        res.send(recibo);
    }).catch(err => {
        if(err.kind === 'ObjectId') {
            return res.status(404).send({
                message: "Note not found with id " + req.params.reciboId
            });                
        }
        return res.status(500).send({
            message: "Error retrieving note with id " + req.params.reciboId
        });
    });
};

// Update a note identified by the noteId in the request
exports.update = (req, res) => {
    // Validate Request
    if(!req.body.content) {
        return res.status(400).send({
            message: "Note content can not be empty"
        });
    }

    // Find note and update it with the request body
    User.findByIdAndUpdate(req.params.reciboId, {
        title: req.body.title || "Untitled Note",
        content: req.body.content
    }, {new: true})
    .then(recibo => {
        if(!recibo) {
            return res.status(404).send({
                message: "Note not found with id " + req.params.reciboId
            });
        }
        res.send(recibo);
    }).catch(err => {
        if(err.kind === 'ObjectId') {
            return res.status(404).send({
                message: "Note not found with id " + req.params.reciboId
            });                
        }
        return res.status(500).send({
            message: "Error updating note with id " + req.params.reciboId
        });
    });
}

// Delete a note with the specified noteId in the request
exports.delete = (req, res) => {
    Recibo.findByIdAndRemove(req.params.reciboId)
    .then(recibo => {
        if(!recibo) {
            return res.status(404).send({
                message: "Note not found with id " + req.params.reciboId
            });
        }
        res.send({message: "Note deleted successfully!"});
    }).catch(err => {
        if(err.kind === 'ObjectId' || err.name === 'NotFound') {
            return res.status(404).send({
                message: "Note not found with id " + req.params.reciboId
            });                
        }
        return res.status(500).send({
            message: "Could not delete note with id " + req.params.reciboId
        });
    });
};

