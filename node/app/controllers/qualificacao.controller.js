const Qualificacao = require('../models/qualificacao.js');

const mongoose = require('mongoose');


const entidadecertificacao = require('../models/entidadecertificacao.js');

// Create and Save a new Note
exports.create = (req, res) => {
    // Validate request
    if(!req.body.nome) {
        return res.status(400).send({
            nome: "Note content can not be lol"
        });
    }

    entidadecertificacao.findOne({_id: req.body.idEntidadeCerticadora}, function(err, result) {
        if (err) {
            throw err};
        console.log(result._id);

    // Create a Note
    const qualificacao = new Qualificacao({
        _id: mongoose.Types.ObjectId(),
        idEntidadeCerticadora: result._id,
        nome: req.body.nome, 
        descricao: req.body.descricao
    });

    // Save Note in the database
    qualificacao.save()
    .then(data => {
        res.send(data);
    }).catch(err => {
        res.status(500).send({
            message: err.message || "Some error occurred while creating the Note."
        });
    });
});
};

// Retrieve and return all notes from the database.
exports.findAll = (req, res) => {
    Qualificacao.find().populate('idEntidadeCerticadora')
    .then(qualificacaos => {
        res.send(qualificacaos);
    }).catch(err => {
        res.status(500).send({
            message: err.message || "Some error occurred while retrieving notes."
        });
    });
};

exports.findOne = (req, res) => {
    Qualificacao.findById(req.params.qualificacaoId)
    .then(qualificacao => {
        if(!qualificacao) {
            return res.status(404).send({
                message: "cliente not found with id " + req.params.qualificacaoId
            });            
        }
        res.send(qualificacao);
    }).catch(err => {
        if(err.kind === 'ObjectId') {
            return res.status(404).send({
                message: "cliente not found with id " + req.params.qualificacaoId
            });                
        }
        return res.status(500).send({
            message: "Error retrieving cliente with id " + req.params.qualificacaoId
        });
    });
};

exports.deleteOne = (req, res) => {
    Qualificacao.remove({_id: req.body.certificacaoId})
    .then(qualificacao => {
        if(!qualificacao) {
            return res.status(404).send({
                message: "Note not found with id " + req.body.certificacaoId
            });
        }
        res.send({message: "Note deleted successfully!"});
    }).catch(err => {
        if(err.kind === 'ObjectId' || err.name === 'NotFound') {
            return res.status(404).send({
                message: "Note not found with id " + req.body.certificacaoId
            });                
        }
        return res.status(500).send({
            message: "Could not delete note with id " + req.body.certificacaoId
        });
    });
};

// Update a note identified by the noteId in the request
exports.update = (req, res) => {
    // Validate Request
    if(!req.body.content) {
        return res.status(400).send({
            message: "cliente content can not be empty"
        });
    }

    // Find note and update it with the request body
    Qualificacao.findByIdAndUpdate(req.params.qualificacaoId, {
        title: req.body.title || "Untitled Note",
        content: req.body.content
    }, {new: true})
    .then(qualificacao => {
        if(!qualificacao) {
            return res.status(404).send({
                message: "Note not found with id " + req.params.qualificacaoId
            });
        }
        res.send(qualificacao);
    }).catch(err => {
        if(err.kind === 'ObjectId') {
            return res.status(404).send({
                message: "Note not found with id " + req.params.qualificacaoId
            });                
        }
        return res.status(500).send({
            message: "Error updating note with id " + req.params.qualificacaoId
        });
    });
}

// Delete a note with the specified noteId in the request
exports.delete = (req, res) => {
    Qualificacao.findByIdAndRemove(req.params.qualificacaoId)
    .then(qualificacao => {
        if(!qualificacao) {
            return res.status(404).send({
                message: "Note not found with id " + req.params.qualificacaoId
            });
        }
        res.send({message: "Note deleted successfully!"});
    }).catch(err => {
        if(err.kind === 'ObjectId' || err.name === 'NotFound') {
            return res.status(404).send({
                message: "Note not found with id " + req.params.qualificacaoId
            });                
        }
        return res.status(500).send({
            message: "Could not delete note with id " + req.params.qualificacaoId
        });
    });
};