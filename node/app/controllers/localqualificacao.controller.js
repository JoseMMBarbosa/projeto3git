const Localqualificacao = require('../models/localqualificacao.js');

const mongoose = require('mongoose');

const Local = require ('../models/local.js');

const Qualificacao = require ('../models/qualificacao.js');

// Create and Save a new Note
exports.create = (req, res) => {
    // Validate request
    if(!req.body.idLocal) {
        return res.status(400).send({
            nome: "Note content can not be lol"
        });
    }

    Local.findOne({_id: req.body.idLocal}, function(err, result) {
        if (err) {
            return res.status(400).send({
                nome: "id produtor nao encontrado"
            });
        }
        console.log(result._id);
        Qualificacao.findOne({_id: req.body.idQualificacao}, function(err, result1) {
            if (err) {
                throw err};
            console.log(result1._id);
    
    // Create a Note
    const localqualificacao = new Localqualificacao({
        _id: mongoose.Types.ObjectId(),
        idLocal: result._id,
        idQualificacao: result1._id

    });

    // Save Note in the database
    localqualificacao.save()
    .then(data => {
        res.send(data);
    }).catch(err => {
        res.status(500).send({
            message: err.message || "Some error occurred while creating the Note."
        });
    });
});
});
};


// Retrieve and return all notes from the database.
exports.findAll = (req, res) => {
    Localqualificacao.find()
    .then(localqualificacaos => {
        res.send(localqualificacaos);
    }).catch(err => {
        res.status(500).send({
            message: err.message || "Some error occurred while retrieving notes."
        });
    });
};

exports.findOne = (req, res) => {
    Localqualificacao.findById(req.params.localqualificacaoId)
    .then(localqualificacao => {
        if(!localqualificacao) {
            return res.status(404).send({
                message: "cliente not found with id " + req.params.localqualificacaoId
            });            
        }
        res.send(localqualificacao);
    }).catch(err => {
        if(err.kind === 'ObjectId') {
            return res.status(404).send({
                message: "cliente not found with id " + req.params.localqualificacaoId
            });                
        }
        return res.status(500).send({
            message: "Error retrieving cliente with id " + req.params.localqualificacaoId
        });
    });
};

// Update a note identified by the noteId in the request
exports.update = (req, res) => {
    // Validate Request
    if(!req.body.content) {
        return res.status(400).send({
            message: "cliente content can not be empty"
        });
    }

    // Find note and update it with the request body
    Localqualificacao.findByIdAndUpdate(req.params.localqualificacaoId, {
        title: req.body.title || "Untitled Note",
        content: req.body.content
    }, {new: true})
    .then(localqualificacao => {
        if(!localqualificacao) {
            return res.status(404).send({
                message: "Note not found with id " + req.params.localqualificacaoId
            });
        }
        res.send(localqualificacao);
    }).catch(err => {
        if(err.kind === 'ObjectId') {
            return res.status(404).send({
                message: "Note not found with id " + req.params.localqualificacaoId
            });                
        }
        return res.status(500).send({
            message: "Error updating note with id " + req.params.localqualificacaoId
        });
    });
}

// Delete a note with the specified noteId in the request
exports.delete = (req, res) => {
    Localqualificacao.findByIdAndRemove(req.params.localqualificacaoId)
    .then(localqualificacao => {
        if(!localqualificacao) {
            return res.status(404).send({
                message: "Note not found with id " + req.params.localqualificacaoId
            });
        }
        res.send({message: "Note deleted successfully!"});
    }).catch(err => {
        if(err.kind === 'ObjectId' || err.name === 'NotFound') {
            return res.status(404).send({
                message: "Note not found with id " + req.params.localqualificacaoId
            });                
        }
        return res.status(500).send({
            message: "Could not delete note with id " + req.params.localqualificacaoId
        });
    });
};