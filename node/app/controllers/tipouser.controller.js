const User = require('../models/users.js');

const mongoose = require('mongoose');

const TipoUser = require('../models/tipouser.js');

// Create and Save a new Note
exports.create = (req, res) => {
    // Validate request
    if(!req.body.tipo) {
        return res.status(400).send({
            message: "Note content can not be empty"
        });
    }

    // create
    const tipouser = new TipoUser({
        _id: mongoose.Types.ObjectId(),
        tipo: req.body.tipo
    });

    // Save Note in the database
    tipouser.save()
    .then(data => {
        res.send(data);
    }).catch(err => {
        res.status(500).send({
            message: err.message || "Some error occurred while creating the Note."
        });
    });
};

// Retrieve and return all notes from the database.
exports.findAll = (req, res) => {
    User.find()
    .then(tipouser => {
        res.send(tipouser);
    }).catch(err => {
        res.status(500).send({
            message: err.message || "Some error occurred while retrieving notes."
        });
    });
};

exports.findOne = (req, res) => {
    User.findById(req.params.tipouserId)
    .then(tipouser => {
        if(!tipouser) {
            return res.status(404).send({
                message: "Note not found with id " + req.params.tipouserId
            });            
        }
        res.send(tipouser);
    }).catch(err => {
        if(err.kind === 'ObjectId') {
            return res.status(404).send({
                message: "Note not found with id " + req.params.tipouserId
            });                
        }
        return res.status(500).send({
            message: "Error retrieving note with id " + req.params.tipouserId
        });
    });
};

// Update a note identified by the noteId in the request
exports.update = (req, res) => {
    // Validate Request
    if(!req.body.content) {
        return res.status(400).send({
            message: "Note content can not be empty"
        });
    }

    // Find note and update it with the request body
    TipoUser.findByIdAndUpdate(req.params.tipouserId, {
        title: req.body.title || "Untitled Note",
        content: req.body.content
    }, {new: true})
    .then(tipouser => {
        if(!tipouser) {
            return res.status(404).send({
                message: "Note not found with id " + req.params.tipouserId
            });
        }
        res.send(tipouser);
    }).catch(err => {
        if(err.kind === 'ObjectId') {
            return res.status(404).send({
                message: "Note not found with id " + req.params.tipouserId
            });                
        }
        return res.status(500).send({
            message: "Error updating note with id " + req.params.tipouserId
        });
    });
}

// Delete a note with the specified noteId in the request
exports.delete = (req, res) => {
    User.findByIdAndRemove(req.params.tipouserId)
    .then(tipouser => {
        if(!tipouser) {
            return res.status(404).send({
                message: "Note not found with id " + req.params.tipouserId
            });
        }
        res.send({message: "Note deleted successfully!"});
    }).catch(err => {
        if(err.kind === 'ObjectId' || err.name === 'NotFound') {
            return res.status(404).send({
                message: "Note not found with id " + req.params.tipouserId
            });                
        }
        return res.status(500).send({
            message: "Could not delete note with id " + req.params.tipouserId
        });
    });
};