const Categoria = require('../models/categoria.js');

const mongoose = require('mongoose');

// Create and Save a new Note
exports.create = (req, res) => {
    // Validate request
    if(!req.body.nome) {
        return res.status(400).send({
            nome: "Note content can not be lol"
        });
    }

    // Create a Note
    const categoria = new Categoria({
        _id: mongoose.Types.ObjectId(),
        nome: req.body.nome, 
        descricao: req.body.descricao
    });

    // Save Note in the database
    categoria.save()
    .then(data => {
        res.send(data);
    }).catch(err => {
        res.status(500).send({
            message: err.message || "Some error occurred while creating the Note."
        });
    });
};

// Retrieve and return all notes from the database.
exports.findAll = (req, res) => {
    Categoria.find()
    .then(categorias => {
        res.send(categorias);
    }).catch(err => {
        res.status(500).send({
            message: err.message || "Some error occurred while retrieving notes."
        });
    });
};

exports.findOne = (req, res) => {
    Categoria.findById(req.params.categoriaId)
    .then(categoria => {
        if(!categoria) {
            return res.status(404).send({
                message: "cliente not found with id " + req.params.categoriaId
            });            
        }
        res.send(categoria);
    }).catch(err => {
        if(err.kind === 'ObjectId') {
            return res.status(404).send({
                message: "cliente not found with id " + req.params.categoriaId
            });                
        }
        return res.status(500).send({
            message: "Error retrieving cliente with id " + req.params.categoriaId
        });
    });
};

exports.deleteOne = (req, res) => {
    Categoria.remove({_id: req.body.categoriaId})
    .then(categoria => {
        if(!categoria) {
            return res.status(404).send({
                message: "Note not found with id " + req.body.categoriaId
            });
        }
        res.send({message: "Note deleted successfully!"});
    }).catch(err => {
        if(err.kind === 'ObjectId' || err.name === 'NotFound') {
            return res.status(404).send({
                message: "Note not found with id " + req.body.categoriaId
            });                
        }
        return res.status(500).send({
            message: "Could not delete note with id " + req.body.categoriaId
        });
    });
};

// Update a note identified by the noteId in the request
exports.update = (req, res) => {
    // Validate Request
    if(!req.body.content) {
        return res.status(400).send({
            message: "cliente content can not be empty"
        });
    }

    // Find note and update it with the request body
    Categoria.findByIdAndUpdate(req.params.categoriaId, {
        title: req.body.title || "Untitled Note",
        content: req.body.content
    }, {new: true})
    .then(categoria => {
        if(!categoria) {
            return res.status(404).send({
                message: "Note not found with id " + req.params.categoriaId
            });
        }
        res.send(categoria);
    }).catch(err => {
        if(err.kind === 'ObjectId') {
            return res.status(404).send({
                message: "Note not found with id " + req.params.categoriaId
            });                
        }
        return res.status(500).send({
            message: "Error updating note with id " + req.params.categoriaId
        });
    });
}

// Delete a note with the specified noteId in the request
exports.delete = (req, res) => {
    Categoria.findByIdAndRemove(req.params.categoriaId)
    .then(categoria => {
        if(!categoria) {
            return res.status(404).send({
                message: "Note not found with id " + req.params.categoriaId
            });
        }
        res.send({message: "Note deleted successfully!"});
    }).catch(err => {
        if(err.kind === 'ObjectId' || err.name === 'NotFound') {
            return res.status(404).send({
                message: "Note not found with id " + req.params.categoriaId
            });                
        }
        return res.status(500).send({
            message: "Could not delete note with id " + req.params.categoriaId
        });
    });
};