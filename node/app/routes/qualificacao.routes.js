module.exports = (app) => {
    const qualificacao = require('../controllers/qualificacao.controller.js');

    // Create a new Note
    app.post('/qualificacao', qualificacao.create);

    // Retrieve all Notes
    app.get('/qualificacao', qualificacao.findAll);

    app.post('/qualificacao/deleteOne', qualificacao.deleteOne);

    // Retrieve a single Note with noteId
    app.get('/qualificacao/:qualificacaoId', qualificacao.findOne);

    // Update a Note with noteId
    app.put('/qualificacao/:qualificacaoId', qualificacao.update);

    // Delete a Note with noteId
    app.delete('/qualificacao/:qualificacaoId', qualificacao.delete);
}