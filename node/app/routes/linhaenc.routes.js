module.exports = (app) => {
    const linhaenc = require('../controllers/linhaenc.controller.js');

    // Create a new Note
    app.post('/linhaenc', linhaenc.create);

    // Retrieve all Notes
    app.get('/linhaenc', linhaenc.findAll);

    app.post('/linhaenc/linhaProdutor', linhaenc.linhaProdutor);


    // Retrieve a single Note with noteId
    app.get('/linhaenc/:linhaencId', linhaenc.findOne);

    // Update a Note with noteId
    app.put('/linhaenc/:linhaencId', linhaenc.update);

    // Delete a Note with noteId
    app.delete('/linhaenc/:linhaencId', linhaenc.delete);
}