module.exports = (app) => {
    const users = require('../controllers/users.controller.js');

    // Create a new Note
    app.post('/usersCliente', users.createCliente);

    app.post('/usersProdutor', users.CreateProdutor);

    app.post('/usersAd', users.createAdmin);

    app.post('/login', users.login);

    app.get('/data', users.data);

    app.get('/users/clientes', users.findAllClientes);

    app.get('/users/admins', users.findAllAdmins);

    app.post('/users/deleteOne', users.deleteOne);

    app.post('/users/deleteOneAdmin', users.deleteOneAdmin);

    // Retrieve all Users
    app.get('/users', users.findAll);

    // Retrieve a single Note with noteId
    app.get('/users/:usersId', users.findOne);

    // Update a Note with noteId
    app.put('/users/:userseId', users.update);

    // Delete a Note with noteId
    app.delete('/users/:usersId', users.delete);
}