module.exports = (app) => {
    const tipoproduto = require('../controllers/tipoproduto.controller.js');

    // Create a new Note
    app.post('/tipoProduto', tipoproduto.create);

    // Retrieve all Users
    app.get('/tipoProduto', tipoproduto.findAll);

    app.post('/tipoProduto/deleteOne', tipoproduto.deleteOne);

    // Retrieve a single Note with noteId
    app.get('/tipoproduto/:tipoprodutoId', tipoproduto.findOne);

    // Update a Note with noteId
    app.put('/tipoproduto/:tipoprodutoId', tipoproduto.update);

    // Delete a Note with noteId
    app.delete('/tipoproduto/:tipoprodutoId', tipoproduto.delete);
}