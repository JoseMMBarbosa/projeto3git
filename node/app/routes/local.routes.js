module.exports = (app) => {
    const local = require('../controllers/local.controller.js');

    // Create a new Note
    app.post('/local', local.create);

    // Retrieve all Notes
    app.get('/local', local.findAll);

    app.post('/local/deleteOne', local.deleteOne);

    // Retrieve a single Note with noteId
    app.get('/local/:localId', local.findOne);

    // Update a Note with noteId
    app.put('/local/:localId', local.update);

    // Delete a Note with noteId
    app.delete('/local/:localId', local.delete);
}