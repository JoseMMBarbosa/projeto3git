module.exports = (app) => {
    const cliente = require('../controllers/cliente.controller.js');

    // Create a new Note
    app.post('/cliente', cliente.create);

    // Retrieve all Notes
    app.get('/cliente', cliente.findAll);

    // Retrieve a single Note with noteId
    app.get('/cliente/:clienteId', cliente.findOne);

    // Update a Note with noteId
    app.put('/cliente/:clienteId', cliente.update);

    // Delete a Note with noteId
    app.delete('/cliente/:clienteId', cliente.delete);
}