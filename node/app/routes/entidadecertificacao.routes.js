module.exports = (app) => {
    const categoria = require('../controllers/entidadecertificacao.controller.js');

    // Create a new Note
    app.post('/entidadecertificacao', categoria.create);

    // Retrieve all Notes
    app.get('/entidadecertificacao', categoria.findAll);

    app.post('/entidadecertificacao/deleteOne', categoria.deleteOne);

    // Retrieve a single Note with noteId
    app.get('/entidadecertificacao/:categoriaId', categoria.findOne);

    // Update a Note with noteId
    app.put('/entidadecertificacao/:categoriaId', categoria.update);

    // Delete a Note with noteId
    app.delete('/entidadecertificacao/:categoriaId', categoria.delete);
}