module.exports = (app) => {
    const encomenda = require('../controllers/encomenda.controller.js');

    // Create a new Note
    app.post('/encomenda', encomenda.create);

    // Retrieve all Notes
    app.get('/encomenda', encomenda.findAll);

    app.post('/encomenda/encomendaCliente', encomenda.encomendaCliente);

    // Retrieve a single Note with noteId
    //app.get('/encomenda/:encomendaId', encomenda.findOne);

    // Update a Note with noteId
    app.put('/encomenda/:encomendaId', encomenda.update);

    // Delete a Note with noteId
    app.delete('/encomenda/:encomendaId', encomenda.delete);
}