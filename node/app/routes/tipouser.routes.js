module.exports = (app) => {
    const tipouser = require('../controllers/tipouser.controller.js');

    // Create a new Note
    app.post('/tipoUser', tipouser.create);

    // Retrieve all Users
    app.get('/tipoUser', tipouser.findAll);

    // Retrieve a single Note with noteId
    app.get('/tipoUser/:tipouserId', tipouser.findOne);

    // Update a Note with noteId
    app.put('/tipouser/:tipouserId', tipouser.update);

    // Delete a Note with noteId
    app.delete('/tipouser/:tipouserId', tipouser.delete);
}