module.exports = (app) => {
    const produtor = require('../controllers/produtor.controller.js');

    // Create a new Note
    app.post('/produtor', produtor.create);

    // Retrieve all Notes
    app.get('/produtor', produtor.findAll);

    app.post('/produtor/deleteOne', produtor.deleteOne);

    app.put('/produtor/updadeProdutor', produtor.updateFinal);

    // Retrieve a single Note with noteId
    app.get('/produtor/:produtorId', produtor.findOne);

    // Update a Note with noteId
    app.put('/produtor/:produtorId', produtor.update);

    // Delete a Note with noteId
    app.delete('/produtor/:produtorId', produtor.delete);
}