module.exports = (app) => {
    const categoria = require('../controllers/categoria.controller.js');

    // Create a new Note
    app.post('/categoria', categoria.create);

    // Retrieve all Notes
    app.get('/categoria', categoria.findAll);

    app.post('/categoria/deleteOne', categoria.deleteOne);

    // Retrieve a single Note with noteId
    app.get('/categoria/:categoriaId', categoria.findOne);

    // Update a Note with noteId
    app.put('/categoria/:categoriaId', categoria.update);

    // Delete a Note with noteId
    app.delete('/categoria/:categoriaId', categoria.delete);
}