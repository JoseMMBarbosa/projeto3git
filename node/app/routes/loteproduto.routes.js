module.exports = (app) => {
    const loteproduto = require('../controllers/loteproduto.controller.js');

    // Create a new Note
    app.post('/loteproduto', loteproduto.create);

    // Retrieve all Notes
    app.get('/loteproduto', loteproduto.findAll);

    app.post('/loteproduto/deleteOne', loteproduto.deleteOne);

    app.post('/loteproduto/produtorProdutor', loteproduto.produtorProdutor);

    app.put('/loteproduto/updateProduto', loteproduto.updateProduto);

    app.post('/loteproduto/tipoProduto', loteproduto.produtoTipoProduto);
    
    app.post('/loteproduto/nome', loteproduto.produtoNome);   

    // Retrieve a single Note with noteId
    app.get('/loteproduto/:loteprodutoId', loteproduto.findOne);

    // Update a Note with noteId
    app.put('/loteproduto/:loteprodutoId', loteproduto.update);

    // Delete a Note with noteId
    app.delete('/loteproduto/:loteprodutoId', loteproduto.delete);
}