module.exports = (app) => {
    const subcategoria = require('../controllers/subcategoria.controller.js');

    // Create a new Note
    app.post('/subcategoria', subcategoria.create);

    // Retrieve all Notes
    app.get('/subcategoria', subcategoria.findAll);

    app.post('/subcategoria/deleteOne', subcategoria.deleteOne);

    // Retrieve a single Note with noteId
    app.get('/subcategoria/:subcategoriaId', subcategoria.findOne);

    // Update a Note with noteId
    app.put('/subcategoria/:subcategoriaId', subcategoria.update);

    // Delete a Note with noteId
    app.delete('/subcategoria/:subcategoriaId', subcategoria.delete);
}