module.exports = (app) => {
    const recibo = require('../controllers/recibo.controller.js');

    // Create a new Note
    app.post('/recibo', recibo.create);

    // Retrieve all Users
    app.get('/recibo', recibo.findAll);

    // Retrieve a single Note with noteId
    app.get('/recibo/:reciboId', recibo.findOne);

    // Update a Note with noteId
    app.put('/recibo/:reciboId', recibo.update);

    // Delete a Note with noteId
    app.delete('/recibo/:reciboId', recibo.delete);
}