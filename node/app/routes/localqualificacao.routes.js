module.exports = (app) => {
    const localqualificacao = require('../controllers/localqualificacao.controller.js');

    // Create a new Note
    app.post('/localqualificacao', localqualificacao.create);

    // Retrieve all Notes
    app.get('/localqualificacao', localqualificacao.findAll);

    // Retrieve a single Note with noteId
    app.get('/localqualificacao/:localqualificacaoId', localqualificacao.findOne);

    // Update a Note with noteId
    app.put('/localqualificacao/:localqualificacaoId', localqualificacao.update);

    // Delete a Note with noteId
    app.delete('/localqualificacao/:localqualificacaoId', localqualificacao.delete);
}